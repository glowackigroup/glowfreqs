#!/usr/bin/python
import sys
import re

def gaulogread(ntotqm,qmjob_prefix):
# This module extracts geometry, hessian, and gradients from a Gaussian03 output file (qmjob_prefix*.log) 
# Results are printed to a file *.info with a format that may be used for the projected frequency calculation
 f=open('%s%s'%(qmjob_prefix,'.log'),'r')
 fh=open(('%s%s'%(qmjob_prefix,'.info')),'w')
 fh.write('%s%s'%(qmjob_prefix,'.log: '))
 fh.write('Number of atoms\n')
 fh.write(str(ntotqm))
 fh.write('\n')
 while 1:
  line=f.readline()
  if not line:break
#pull out & print the Input orientation
  elif re.search("Input orientation",line):
    input_orient = make_array(ntotqm,4) 
    f.readline()
    f.readline()
    f.readline()
    f.readline()
    for i in range(ntotqm):
      line=f.readline().split()
      linex=float(line[3])
      input_orient[i][1]=linex
      liney=float(line[4])
      input_orient[i][2]=liney
      linez=float(line[5])
      input_orient[i][3]=linez
#pull out & print the Distance matrix atoms
  elif re.search("Distance matrix",line):
    atom_list = make_array(ntotqm,2) 
    f.readline()
    for i in range(ntotqm):
      line=f.readline().split()
      atom_list[i][1]=line[1]
#pull out & print the Standard orientation; print warning if it's diff to the Input orientation
  elif re.search("Standard orientation",line):
    fh.write('%s%s'%(qmjob_prefix,'.log: ')) 
    fh.write('Standard orientation (Bohr)\n')
    f.readline()
    f.readline()
    f.readline()
    f.readline()
    for i in range(ntotqm):
      line=f.readline().split()
      fh.write(str(atom_list[i][1]).ljust(3))
      linex=float(line[3])
      fh.write(str(linex).rjust(20))
      if linex!=input_orient[i][1]:
        print '%s%s'%(qmjob_prefix,'.log'), "Warning: Input & Standard Orientation are not identical!!"
      liney=float(line[4])
      fh.write(str(liney).rjust(20))
      if liney!=input_orient[i][2]:
        print '%s%s'%(qmjob_prefix,'.log'), "Warning: Input & Standard Orientation are not identical!!"
      linez=float(line[5])
      fh.write(str(linez).rjust(20))
      if linez!=input_orient[i][3]:
        print '%s%s'%(qmjob_prefix,'.log'), "Warning: Input & Standard Orientation are not identical!!"
      fh.write('\n') 
#pull out the gradients
  elif line[:59]==' Center     Atomic                   Forces (Hartrees/Bohr)':
    gradients = make_array(ntotqm,4)  
    f.readline()
    f.readline()
    for i in range(ntotqm):
      line=f.readline().split()
      linex=float(line[2])*-1
      gradients[i][1]=linex
      liney=float(line[3])*-1
      gradients[i][2]=liney
      linez=float(line[4])*-1
      gradients[i][3]=linez
#pull out the route block containing the hessian by searching for 1\1\
  elif re.search("1\\\\1\\\\", line):
    line=f.readline()
    resl=line.strip()
    while 1:  
      line=f.readline()
      if not line:break
      resl=resl+line.strip()
 f.close()
#order the hessian, assign it to an array, and print  
 for i in range(len(resl)):
  if resl[i:i+5]=='NImag':
     res=resl[i+6:]
     break
 for i in range(len(res)):
   if res[i:i+1]=='\\':
     resl=res[i+2:]
     break
 res=resl.split(',')
 qhes=[[] for i in range(ntotqm*3+1)]
 ip=0
 for i in range(ntotqm*3):
   for j in range(i+1):
     if '\\' in res[ip]:
       hlp=res[ip]
       for k in range(len(res[ip])):
         if hlp[k:k+1]=='\\':
           res[ip]=hlp[:k-1]
     qhes[i+1].append(str(res[ip]))  
     ip=ip+1
 for i in range(ntotqm*3):
    qhes[i+1].insert(0,[])
 indx=0
 fh.write('%s%s'%(qmjob_prefix,'.log: '))
 fh.write('Hessian (Hartree/Bohr^2)\n')
 for i in range(ntotqm*3):
    indx=indx+1
    hlp=0
    fh.write(str(indx).rjust(4))
    inum=0         
    for j in range(i+1):
       inum=inum+1
       hlp=hlp+1
       fh.write(str(qhes[i+1][j+1]).rjust(20))
       if hlp==4 and inum!=i+1:
         fh.write('\n')
         fh.write(str(indx).rjust(4))         
         hlp=0
    fh.write('\n')
#print the gradients
 fh.write('%s%s'%(qmjob_prefix,'.log: '))
 fh.write('Gradient (Hartree/Bohr)')
 fh.write('\n')
 for i in range(ntotqm):
   fh.write(str(atom_list[i][1]).ljust(3))
   fh.write(str(gradients[i][1]).rjust(20))
   fh.write(str(gradients[i][2]).rjust(20))
   fh.write(str(gradients[i][3]).rjust(20))
   fh.write('\n')  
 fh.close()

# here's a function to construct an array
def make_array(r,c):
  a=[]
  for i in range(r):
    a.append([])
    for ii in range(c):
      a[i].append(0)
  return a

if __name__ == "__main__":
#  print sys.argv
  gaulogread( int(sys.argv[1]), sys.argv[2] )


# this python script may be run to analyze a gaussian *.log file as follows:
# python gauread.py n string
# where n is the number of atoms
# and the g03 output has the name string.log   
