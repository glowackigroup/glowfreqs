David Glowacki, Bristol University, Feb 2010
updated November 2015

glowfreq program README


How to carry out the vibrational analysis at the MECP using the glowfreq program:

1) compile glowfreq by untarring it, going to /src, and typing "make"
The executable will be located in the top level directory.  if you do not    
have the pgf90 fortran compiler, then you will need to change the flags
FF and LD in the Makefile so that they correspond to the f90 compiler 
available on your machine

2) Do a g03 frequency calculation for each state at the MECP geometry, and be 
sure that the input orientation is identical to gaussian's standard orientation.
This should generate 2 different *.log files

3) For each input file, run the python script "gauread.py" as follows:

python gauread.py n whateverprefix

where n is the number of atoms in the molecule, and the *.log files are named 
as whateverprefix.log

4) cat together the *.info files generated in step 3 into a single file:

cat fileprefix1.info fileprefix2.info > filename_whatever

5) run the executable that performs the MECP vibrational analysis on the file
generated above as follows:

./glowfreq filename_whatever

6) two output files will be generated, *.ROVIBprop and *.MECPprop. These include 
the information required for subsequent spin forbidden tunnelling corrected RRKM & 
TST calculations, which may be carried out in MESMER (sourceforge.net/projects/mesmer/).
*.ROVIBprop is MOLDEN readable so that you can visualize the frequencies obtained in 
the vibrational analysis

Note: when using this program, please cite the following: 

Gannon, Glowacki et al., Faraday Discussions, 2010, 147, 173-188

If you use it in conjunction with MESMER (http://sourceforge.net/projects/mesmer/), 
then please additionally cite:

Plane, ..., Glowacki, et al., Journal of Chemical Physics, 2012, 137, 014310