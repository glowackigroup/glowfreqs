!
!  StateAvgHessian.f90
!  glowfreq
!
!  Created by David Glowacki on 03/02/2010.
!  Copyright 2010 University of Bristol. All rights reserved.
!

SUBROUTINE StateAvgHessian(N,H1,H2,H3,lambda)
 implicit none

 INTEGER, INTENT(IN) :: N
 DOUBLE PRECISION, INTENT(IN) :: lambda
 double precision, dimension(N,N),intent(in) :: H1, H2
 double precision, dimension(N,N),intent(out) :: H3

 H3 = H1 - lambda * (H1 - H2)

END SUBROUTINE StateAvgHessian
