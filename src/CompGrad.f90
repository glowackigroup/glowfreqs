!
!  CompGrad.f90
!  glowfreq
!
!  Created by David Glowacki on 03/02/2010.
!  Copyright 2010 University of Bristol. All rights reserved.
!

SUBROUTINE CompGrad(N,G1,G2,G3,lambda,stddev)
 implicit none

        ! this routine takes the gradients and calculates the
        ! Lagrangian Constraint factor lambda.
        ! lambda is derived by weighted averaging of G1 / (G1 - G2)
        ! over all the components of the G's
        ! the weight is (ABS(G1(ij))*ABS(G2(ij)))**0.5
        ! it also output a new gradient G3 which is G1 - G2 

INTEGER, INTENT(IN) :: N
DOUBLE PRECISION,DIMENSION(N),INTENT(IN) :: G1,G2
DOUBLE PRECISION,DIMENSION(N),INTENT(OUT) :: G3
DOUBLE PRECISION,DIMENSION(N) :: Ratio,Weight
INTEGER :: I,J
DOUBLE PRECISION :: epsilon=0.00001,dev_add=0.,SumWeight=0.,SumLambda=0.
DOUBLE PRECISION, INTENT(OUT) :: lambda,stddev

    DO I=1,N
       IF (ABS(G1(I))<epsilon .or. ABS(G2(I))<epsilon) THEN
           Ratio(I)=0.
           Weight(I)=0.
       ELSE
           Ratio(I) = G1(I) / (G1(I) - G2(I))
           Weight(I) = (ABS(G2(I)) * ABS(G1(I)))**0.5
           SumWeight = SumWeight + Weight(I)
           SumLambda = SumLambda + Weight(I) * Ratio(I)
       END IF
    END DO

    lambda = SumLambda / SumWeight
    G3 = G1 - G2

    DO I=1,N
        IF (Ratio(I) /= 0.) THEN
          dev_add = dev_add + Weight(I) * ((Ratio(I) - lambda) ** 2)
        END IF
    END DO

    stddev = (dev_add / SumWeight) ** 0.5
         ! calculate the standard deviation of lambda

END SUBROUTINE CompGrad

