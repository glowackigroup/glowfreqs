!
!  glowfreq: program for performing vibrational frequency analysis at a 
!  minimum energy crossing point (MECP) between 2 adiabatic PESs
!
!  It reads in a file that contains # atoms, geometries, gradient, and 
!  hessian for 2 adiabatic states at a minimum energy crossing point
!
!  Created by David Glowacki on 03/02/2010.
!  Jan 2010 University of Bristol. All rights reserved.
!

PROGRAM MECP_vibrationalAnalysis
implicit none

	! This program reads a hessian file and cart coords (in Angstrom) from a file
	! called 'hessian_input'.
	! It calculates vibrational frequencies.

INTEGER :: ns,n,nsA,nA,nsB,nB,i, j,k,jj,ll,l,numargs,iargc
DOUBLE PRECISION :: lam,sdev,rmass,F,dF
character(len=2), allocatable :: at(:)
INTEGER, ALLOCATABLE :: anum(:)
DOUBLE PRECISION, ALLOCATABLE :: aw(:),h(:,:),eigvx(:,:),eigs(:),grd(:)
DOUBLE PRECISION, ALLOCATABLE :: qA(:),hA(:,:),grdA(:),qB(:),hB(:,:),grdB(:),cmq(:,:)
DOUBLE PRECISION, ALLOCATABLE :: freqau(:),freqwn(:),eigv(:,:),pmhes(:,:),rotconsts(:)
CHARACTER(LEN=10), ALLOCATABLE :: imag(:)
CHARACTER*40 :: inputfile,output1,output2
DOUBLE PRECISION, PARAMETER :: amu_to_SI=9.3758367d29, c_in_cm=2.99792458d10, amu_to_au=1.822887d3, ang_to_bohr=1.889725989d0
DOUBLE PRECISION, PARAMETER :: amub2tocm=60.1997601

CHARACTER(LEN=2), allocatable :: lab(:),labA(:),labB(:)

write (*,*) " "

! get the input filename
numargs = iargc()
if(numargs.eq.0) then
  write(*,*),'glowfreq cannot find an inputfile argument!!!! ...terminating execution'
  stop
elseif(numargs.gt.1) then
  write(*,*),'glowfreq finds more than one inputfile argument!!!! ...terminating execution'
  stop
elseif(numargs.eq.1) then
  call getarg(1,inputfile)
  write(*,*),'glowfreq will analyze data contained in ', inputfile
  write (*,*) " "
endif

OPEN(UNIT=8,FILE=inputfile)

write (*,*) "reading input for state A..."

! read info for state A
call read_dimensions(inputfile,nsA)

nA=3*nsA
allocate(qA(nA),hA(nA,nA),aw(nA),labA(nA),grdA(nA),anum(nsA),cmq(nsA,3))
CALL ReadInput_prj(inputfile,nsA,nA,qA,hA,aw,labA,grdA,anum)

write (*,*) "reading input for state B..."

! read info for state B
call read_dimensions(inputfile,nsB)

nB=3*nsB
allocate(qB(nB),hB(nB,nB),labB(nB),grdB(nB))
CALL ReadInput_prj(inputfile,nsB,nB,qB,hB,aw,labB,grdB,anum)

write (*,*) "Finished reading data for both states..."
CLOSE(8)

! hesstran interface: check that the geometries & number of atoms are the same

write (*,*) "checking that the geometries for both states are identical..."
write (*,*) " "
call CheckGeom(nA,nB,qA,qB)

! if they're the same, define n and allocate memory for arrays

n=nA
allocate(h(n,n),at(ns),eigvx(n,n),eigs(n),grd(n),pmhes(n,n))
allocate(freqau(n),freqwn(n),imag(n),lab(ns),eigv(n,n),rotconsts(3))

! hesstran interface: compares the two gradients, extracts the lambda coeff, and average the
! two gradients to give the new G3

write (*,*) "computing state averaged gradient along the MECP crossing seam..."
call CompGrad(n,grdA,grdB,grd,lam,sdev)

! hesstran interface: calculate state averaged Hessian

write (*,*) "computing state averaged Hessian, h..."
write (*,*) " "
call StateAvgHessian(n,hA,hB,h,lam)

! hesstran interface: calculate reduced mass, gradient norms & difference gradient

call ReducedMass(n,grdA,grdB,grd,aw,rmass,F,dF)

! hesstran interface: print out some stuff

output1=trim(inputfile)//".MECPprop"
call PrintOutput(output1,grdA,grdB,n,lam,sdev,grd,h,rmass,F,dF)

! Project the state averaged gradient, rotations, and vibrations from the state averaged Hessian.
write (*,*) "Projecting rotations, translations, and state averaged gradient from h..."
call projection(n,grd,h,pmhes,qA,aw,cmq,rotconsts)

write (*,*) "Diagonalizing the projected Hessian..."
write (*,*) " "
CALL Diag(n,pmhes,EigV,eigs)

DO I = 1, N
! eigvx contains mass weighted eigenvectors; eigv contains non-mass weighted vectors
	eigvx(:,I) = eigv(:,I) / aw
	eigvx(:,I) = eigvx(:,I) / SQRT(SUM(eigvx(:,I)**2))
        freqau(I) = SQRT(ABS(eigs(I))/amu_to_au)
        IF (eigs(I) .lt. 0.d0) THEN
                imag(I) = " IMAGINARY"
        ELSE
                imag(I) = ""
        END IF
	freqwn(I) = sqrt(amu_to_SI*amu_to_au)*freqau(I) * 1.0d0/(2.0d0*acos(-1.0d0)*c_in_cm)
END DO

output2=trim(inputfile)//".ROVIBprop"

OPEN(UNIT=8,FILE=output2,ACTION="write")

WRITE (Unit=8,FMT=*) "[Molden Format]"
write (8,*) "Frequencies: (au & cm-1)"
DO J=1,n
        write (UNIT=8,FMT='(F20.10,F20.3,A,A)') freqau(J), freqwn(J),"   ",imag(J)
END DO
write (8,*) ""
write (8,FMT='(A28,F20.10)') "Harmonic Zero-point energy:", .5d0 * SUM(freqau(1:N-6))
write (8,*) ""
write (8,FMT=120) "Rotational Constants (amu*bohr**2): ", rotconsts(1), rotconsts(2), rotconsts(3)
write (8,FMT=110) "Rotational Constants (cm-1): ", amub2tocm/rotconsts(1), amub2tocm/rotconsts(2), amub2tocm/rotconsts(3)
write (8,*) ""
write (8,*)'[Atoms] Angs'
DO I = 1,nsa
		write (8,FMT=130) labA(i), i, anum(i), cmq(i,1)/ang_to_bohr, cmq(i,2)/ang_to_bohr, cmq(i,3)/ang_to_bohr 
END DO
write (8,*)'[FREQ]'
DO J=1,n
        write (UNIT=8,FMT='(F10.2)') freqwn(n-J+1)
END DO
write (8,*)'[FR-COORD]'
DO I = 1,nsa
  write (8,FMT=140) labA(i), cmq(i,1), cmq(i,2), cmq(i,3) 
END DO
write (8,*)'[FR-NORM-COORD]'
do i=1,n
  write(8,FMT='(A9,I28)') 'Vibration', i
  do j=1,nsa
    write(8,FMT=150) EigVX(3*j-2,n-i+1), EigVX(3*j-1,n-i+1), EigVX(3*j,n-i+1)
  enddo
enddo

CLOSE(8)

write (*,*) "glowfreq executed successfully: *.ROVIBprop and *.MECPprop output files created"
write (*,*) "The geometry and frequencies in the *.ROVIBprop file may be visualized using MOLDEN"

110 FORMAT(1X,A28,10X,F11.4,2X,F11.4,2X,F11.4)
120 FORMAT(1X,A36,2X,F11.4,2X,F11.4,2X,F11.4)
130 FORMAT(A2,2X,I3,2X,I3,2X,F13.10,2X,F13.10,2X,F13.10)
140 FORMAT(A2,2X,F13.10,2X,F13.10,2X,F13.10)
150 FORMAT(F21.8,F21.8,F21.8)

END PROGRAM MECP_vibrationalAnalysis


