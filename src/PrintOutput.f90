!
!  Output.f90
!  glowfreq
!
!  Created by David Glowacki on 03/02/2010.
!  Copyright 2010 University of Bristol. All rights reserved.
!

SUBROUTINE PrintOutput(Report,G1,G2,N,lambda,stddev,G3,H3,redmass,F,delF)
 implicit none

  INTEGER, INTENT(IN) :: N
  INTEGER :: I,J,dimH,Nlines,Nextra
  DOUBLE PRECISION, INTENT(IN) :: lambda, stddev, redmass, F, delF
  DOUBLE PRECISION :: k
  double precision, dimension(N),intent(in) :: G1,G2,G3
  double precision, dimension(N,N),intent(in) :: H3
  CHARACTER(LEN=40),INTENT(IN) :: Report
  CHARACTER(LEN=40) :: Temp

  open (unit=9,file=Report)

    write (9,*) "     **************************************************"
    write (9,*) "     *                                                *"
    write (9,*) "     *           Output of a state averaged           *"
    write (9,*) "     *      Hessian from two Hessians at the MECP     *"
    write (9,*) "     *      Using the formula of Koga & Morokuma      *"
    write (9,*) "     *                                                *"
    write (9,*) "     *          J. N. Harvey & D.R. Glowacki          *"
    write (9,*) "     *                                                *"
    write (9,*) "     *            U. of Bristol, feb 2010             *"
    write (9,*) "     *                                                *"
    write (9,*) "     **************************************************"
    write (9,*)
    write (9,*)
    write (9,*)
    write (9,*) "The molecules both have",N/3," atoms"
    write (9,*)
    write (9,*) "Their geometries are identical"
    write (9,*)
    write (9,*) "The lagrangian constraint lambda is"
    write (UNIT=9,FMT=110) " ",lambda
    write (9,*)
    write (9,*) "With a standard deviation of"
    write (UNIT=9,FMT=110) " ",stddev
    write (9,*)
    write (9,*) "By the way, this means that the gradient ratio G1 / G2 ="
    write (UNIT=9,FMT=110) " ",(lambda / (lambda - 1))
    write (9,*)
    write (9,*) "The difference gradient (G1 - G2) in Hartree/bohr is :"
    write (9,*) "$GRAD"
    write (9,*)
    DO I=1,N/3
      write (UNIT=9,FMT=103) " ",G3(3*I-2)," ",G3(3*I-1)," ",G3(3*I)
    END DO
    write (9,*) "$END"
    write (9,*)
    write (9,*) "For comparison, the two initial gradients (Hartree/bohr) were :"
    write (9,*) "$GRAD"
    write (9,*)
    DO I=1,N/3
      write (UNIT=9,FMT=103) " ",G1(3*I-2)," ",G1(3*I-1)," ",G1(3*I)
    END DO
    write (9,*) "$END"
    write (9,*) "$GRAD"
    write (9,*)
    DO I=1,N/3
      write (UNIT=9,FMT=103) " ",G2(3*I-2)," ",G2(3*I-1)," ",G2(3*I)
    END DO
    write (9,*) "$END"
    write (9,*)
    write (9,*) "The Reduced Mass orthogonal to the seam of crossing is (a.m.u.):"
    write (9,'(F10.4)') redmass
    write (9,*) "The geometric mean of the norms of the two gradients is:"
    write (9,'(F16.8)') F
    write (9,*) "The norm of the difference gradient is:"
    write (9,'(F16.8)') delF
    write (9,*)
    write (9,*) "State averaged hessian (not mass weighted, Hartree/bohr^2):"
    write (9,*) "$HESS"
    write (9,*)
    dimH = N
    Nlines = dimH/5
    Nextra = dimH - 5*Nlines 
    DO I=1,dimH
       DO J=1,Nlines
       write (UNIT=9,FMT=104) I,J," ",H3(I,5*J-4)," ",H3(I,5*J-3)," ",H3(I,5*J-2)," ",H3(I,5*J-1)," ",H3(I,5*J)
       END DO

       IF (Nextra==4) THEN
           write (UNIT=9,FMT=104) I,Nlines+1," ",H3(I,dimH-3)," ",H3(I,dimH-2)," ",H3(I,dimH-1)," ",H3(I,dimH)
       ELSE IF (Nextra==3) THEN
           write (UNIT=9,FMT=104) I,Nlines+1," ",H3(I,dimH-2)," ",H3(I,dimH-1)," ",H3(I,dimH)
       ELSE IF (Nextra==2) THEN
           write (UNIT=9,FMT=104) I,Nlines+1," ",H3(I,dimH-1)," ",H3(I,dimH)
       ELSE IF (Nextra==1) THEN
           write (UNIT=9,FMT=104) I,Nlines+1," ",H3(I,dimH)
       END IF

    END DO
    write (9,*) "$END"
    write (9,*) 
    close (9)


103 FORMAT(A18,E17.10,A3,E17.10,A3,E17.10)
104 FORMAT(I2,I3,A1,E14.8,A1,E14.8,A1,E14.8,A1,E14.8,A1,E14.8)
110 FORMAT(A5,E10.4)
    
END SUBROUTINE PrintOutput

