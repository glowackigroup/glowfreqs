SUBROUTINE ReadInput_prj(report,ns,nx,q,h,amx,at,gd,atomicnum)
implicit none

CHARACTER(LEN=40),INTENT(IN) :: report
INTEGER, INTENT(IN) :: ns, nx
INTEGER, INTENT(OUT) :: atomicnum(ns)
DOUBLE PRECISION, INTENT(OUT) :: Q(nx), amx(nx), h(nx,nx),gd(nx)
character(len=2), intent(out) :: at(ns)

character(len=80) :: dummy,atfind
character(len=2) :: labels(83)
integer :: I, ii, J, jj, kk, K
double precision :: masses(83)

labels=(/" H","He","Li","Be"," B"," C"," N"," O"," F","Ne","Na","Mg","Al","Si"," P"," S","Cl","Ar", &
& " K","Ca","Sc","Ti"," V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br", &
& "Kr","Rb","Sr"," Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te"," I","Xe", &
& "Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb", &
&  "Lu","Hf","Ta"," W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi" /)
! massesfor most common isotopes retrieved from
! http://physics.nist.gov/cgi-bin/Compositions/stand_alone.pl?ele=&all=all&ascii=html&isotype=some

!               1H                4He          7Li              9Be                 11B
masses = (/ 1.007825D+00,    4.002603D+00,   7.016003D+00,   9.012183D+00,   11.009305D+00, &
!               12C              14N             16O           19F                20Ne
&     12.000000D+00,   14.003074D+00,  15.994915D+00,  18.998403D+00,   19.992440D+00, &
!              23Na             24Mg            27Al             28Si             31P
&     22.989769D+00,   23.985042D+00,  26.981539D+00,  27.976926D+00,   30.973762D+00, &
!              32S              35Cl            40Ar             39K              40Ca
&     31.972071D+00,   34.968853D+00,  39.962383D+00,  38.963706D+00,   39.962591D+00,&
!              45Sc             48Ti            51V              52Cr             55Mn
&     44.955908D+00,   47.947942D+00,  50.943953D+00,  51.940506D+00,   54.938044D+00,&
!              56Fe             59Co            58Ni             63Cu             64Zn
&     55.934936D+00,   58.933194D0,    57.935342D+00,  62.929598d0,     63.929142d0,  &
!              69Ga             74Ge            75As             80Se             79Br
&     68.925574d0,     73.921177d0,    74.921596d0,    79.916522d0,     78.918338d0,  &
!              84Kr             85Rb            88Sr             89Y             90Zr
&     83.911497d0,     84.911790d0,    87.905613d0,    88.905840d0,     89.904698d0,  &
!              93Nb             98Mo            [Tc]            102Ru             103Rh
&     92.906373d0,     97.905405d0,    98.d0,         101.904344d0,    102.905498d0,  &
!             108Pd            107Ag            114Cd           115In            120Sn
&    105.903480d0,    106.905092d0,   113.903365d0,   114.903879d0,    119.902202d0,   &
!             121Sb            130Te           127I             132Xe            133Cs
&    120.903812d0,    129.906223d0,   126.904472d0,   131.904155d0,    132.905452d0, &
!             138Ba             139La          140Ce            141Pr            142Nd
&    137.905247d0,    138.906356d0,   139.905443d0,   140.907658d0,    141.907729d0, &
!             [Pm]             152Sm           153Eu            158Gd            159Tb
&    145.d0,          151.919740d0,   152.921238d0,   157.924112d0,    158.925355d0, &
!             164Dy            165Ho           166Er            169Tm            174Yb
&    163.929182d0,    164.930329d0,   165.930300d0,   168.934218d0,    173.938867d0, &
!             175Lu            180Hf           181Ta            184W             187Re
&    174.940775d0,    179.946557d0,   180.947996d0,   183.950931d0,    186.955750d0, &
!             192Os            193Ir           195Pt            197Au            202Hg
&    191.961477d0,    192.962922d0,   194.964792d0,   196.965833d0,    201.970643d0, &
!             205Tl            208Pb           209Bi
&    204.974428d0,    207.976653d0,   208.980399d0 /)

OPEN(UNIT=8,FILE=report,POSITION="asis")

read (UNIT=8, FMT=*) dummy
do i =1,ns
	j=3*(i-1)
	read(8,*) at(i), (q(k),k=j+1,j+3)
        atfind='FALSE'  
	do j=1,54
		if (ADJUSTL(at(i)).eq.ADJUSTL(labels(j))) then
			amx(3*i-2)=masses(j)
			amx(3*i-1)=amx(3*i-2)
			amx(3*i)=amx(3*i-2)
            atomicnum(i)=j
            atfind='TRUE'
			exit
		end if
	end do
        IF (atfind.eq.'FALSE') THEN
           write(*,*) 'Atomic mass is not assigned in frequency code for atom  ', i,'   ',at(i)
           stop
        END IF
end do
read (UNIT=8, FMT=*) dummy
do i=1, nx
	j=i/4
	if (j.gt.0) then
	do k=1,j
		read (8,*) kk,(h(i,ii),ii=4*k-3,4*k)
	end do
	end if
	if (i.ne.4*j) then
		read (8,*) kk,(h(i,ii),ii=4*j+1,i)
	end if
	h(1:i-1,i)=h(i,1:i-1)
end do
read (UNIT=8, FMT=*) dummy
do i=1,ns
    ii=(i-1)*3+1
    read (8,*) dummy,(gd(k),k=ii,ii+2)
enddo

END SUBROUTINE ReadInput_prj


