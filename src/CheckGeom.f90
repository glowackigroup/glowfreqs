!
!  CompGeom.f90
!  glowfreq
!
!  Created by David Glowacki on 03/02/2010.
!  Copyright 2010 University of Bristol. All rights reserved.
!

SUBROUTINE CheckGeom(numA,numB,Q1,Q2)
 implicit none

        ! from the geometries, this subroutine establishes whether they are identical
        ! otherwise it returns error=-3

INTEGER, INTENT(IN) :: numA,numB
DOUBLE PRECISION,INTENT(OUT) :: Q1(numA),Q2(numB)
INTEGER :: I,J
DOUBLE PRECISION :: epsilon=1.d-6

  IF(numA.ne.numB)  THEN
      write (*,*) "The molecules have different numbers of atoms!!!"
      stop    
  ENDIF

  DO I=1,NumA
    IF (ABS(Q1(NumA)-Q2(NumA))>epsilon) THEN
      write (*,*) "The geometry of these two molecules is not identical"
      write (*,*) "Please check the input files"
      stop 
    END IF
  END DO

END SUBROUTINE CheckGeom
