!
!  ReducedMass.f90
!  glowfreq
!
!  Created by David Glowacki on 03/02/2010.
!  Copyright 2010 University of Bristol. All rights reserved.
!

SUBROUTINE ReducedMass(N,G1,G2,G3,AW,redmass,F,delF)
 IMPLICIT NONE

 INTEGER, INTENT(IN) :: N
 DOUBLE PRECISION, INTENT(IN) :: G1(N), G2(N), G3(N), AW(N)
 DOUBLE PRECISION, INTENT(OUT) :: redmass, F, delF

 INTEGER :: I
 DOUBLE PRECISION :: snmwg, ngr1, ngr3, ndg
 double precision, parameter:: amu_to_au= 1.822887d3

 ndg = 0.d0 ; ngr1 = 0.d0 ; ngr3 = 0.d0 ; snmwg = 0.d0
 do i = 1, N
    ndg = ndg + G3(i)**2
    ngr1 = ngr1 + G1(i)**2
    ngr3 = ngr3 + G2(i)**2
    snmwg = snmwg + G1(i)**2/aw(i)
 end do

 redmass=ngr1/snmwg

 DelF = SQRT(ndg)
 F = SQRT(SQRT(ngr1 * ngr3))

END SUBROUTINE ReducedMass
