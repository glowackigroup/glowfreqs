!
! this routine takes as input the following: 
! nx = number of atoms
! gradient = non-mass weighted gradient along rxn coordinate
! hess = non-mass weighted hessian
! pmhess = matrix in which the projected & mass weighted hessian will be placed
! q = vector of atomic coordinates
! mass3d vector of atomic masses
!
! It projects from the mass weighted hessian a gradient, 3 rotations & 3 translations
! using the method described by Miller et al. JCP, 72(1), 1980, p 99
!
! D Glowacki, U of Bristol, feb 2010
!
SUBROUTINE projection(nx,gradient,hess,pmhess,q,mass3d,CMcoord,rotationalcs)
implicit none

INTEGER, INTENT(IN) :: nx
DOUBLE PRECISION, INTENT(IN) :: q(nx),gradient(nx),mass3d(nx),hess(nx,nx),rotationalcs(3)
DOUBLE PRECISION, INTENT(OUT) :: pmhess(nx,nx),CMcoord(nx/3,3)
integer :: i, J, k, atoms, alpha, dimns, beta, natoms, LeviC,dummy
DOUBLE PRECISION :: mwhess(nx,nx),dr_drt(nx,nx),unim(nx,nx),pmat(nx,nx)
DOUBLE PRECISION :: mass1d(nx/3),coord(nx/3,3)
DOUBLE PRECISION :: ICMtens(3,3),EigVI(3,3),lamMat(3,3),EigInv(3,3),invsqICM(3,3)
DOUBLE PRECISION :: mwgrad(nx), mwXROT(nx), mwYROT(nx), mwZROT(nx), mwXTRANS(nx), mwYTRANS(nx), mwZTRANS(nx)
DOUBLE PRECISION :: nmwgrad(nx),nmwXROT(nx),nmwYROT(nx),nmwZROT(nx),nmwXTRANS(nx),nmwYTRANS(nx),nmwZTRANS(nx)
DOUBLE PRECISION :: totalMass,cmx=0.0d0,cmy=0.0d0,cmz=0.0d0
DOUBLE PRECISION, PARAMETER :: ZERO=0.00D+00, ang_to_bohr=1.889725989d0

! initialize some things
natoms=nx/3
ICMtens=ZERO
CMcoord=ZERO
mwXROT=ZERO
mwYROT=ZERO
mwZROT=ZERO
mwXTRANS=ZERO
mwYTRANS=ZERO
mwZTRANS=ZERO

! create a 1d vector of the masses from the 3d vector of masses
do i=1,natoms
  mass1d(i)=mass3d(3*i-2)
enddo
totalMass=sum(mass1d)

! create a matrix of the coordinates in bohr
k=1
do i=1,natoms
  do j=1,3
    coord(i,j)=q(k)*ang_to_bohr
    k=k+1
  enddo
enddo

! calculate the center of mass
do i=1,natoms
  cmx=cmx+mass1d(i)*coord(i,1)
  cmy=cmy+mass1d(i)*coord(i,2)
  cmz=cmz+mass1d(i)*coord(i,3)
enddo

cmx=cmx/totalMass
cmy=cmy/totalMass
cmz=cmz/totalMass

! translate to the center of mass
do i=1,natoms
  CMcoord(i,1)=coord(i,1)-cmx
  CMcoord(i,2)=coord(i,2)-cmy
  CMcoord(i,3)=coord(i,3)-cmz
enddo

! calculate inertial tensor matrix

do i=1,natoms
  ICMtens(1,1)=ICMtens(1,1)+mass1d(i)*((CMcoord(i,2))**2+(CMcoord(i,3))**2);
  ICMtens(2,2)=ICMtens(2,2)+mass1d(i)*((CMcoord(i,1))**2+(CMcoord(i,3))**2);
  ICMtens(3,3)=ICMtens(3,3)+mass1d(i)*((CMcoord(i,1))**2+(CMcoord(i,2))**2);
  ICMtens(1,2)=ICMtens(1,2)-mass1d(i)*( CMcoord(i,1)   * CMcoord(i,2));
  ICMtens(1,3)=ICMtens(1,3)-mass1d(i)*( CMcoord(i,1)   * CMcoord(i,3));
  ICMtens(2,3)=ICMtens(2,3)-mass1d(i)*( CMcoord(i,2)   * CMcoord(i,3));
enddo

ICMTens(2,1)=ICMTens(1,2)
ICMTens(3,1)=ICMTens(1,3)
ICMTens(3,2)=ICMTens(2,3)

! diagonalize the inertial Tensor matrix
call Diag(3,ICMTens,EigVI,rotationalcs)

! form a diagonal matrix of D^(-1/2)
lamMat=ZERO
do i=1,3 
  lamMat(i,i)=1.0d0/sqrt(rotationalcs(i))
enddo

! invert the eigenvector matrix
do i=1,3
  do j=1,3
    EigInv(i,j)=EigVI(i,j)
  enddo
enddo

call invert(3,3,EigInv)

! calculate I^(-1/2) = U D^(-1/2) U^(-1)
invsqICM=matmul(EigVI,matmul(lamMat,EigInv))

! calculate the mass weighted othogonal rotation eigenvectors
i=1
do j=1,natoms
  do dimns=1,3
    do alpha=1,3
      do beta=1,3
        mwXROT(i)=mwXROT(i)+invsqICM(1,alpha)*LeviC(alpha,beta,dimns)*CMcoord(j,beta)*sqrt(mass1d(j))
        mwYROT(i)=mwYROT(i)+invsqICM(2,alpha)*LeviC(alpha,beta,dimns)*CMcoord(j,beta)*sqrt(mass1d(j))
        mwZROT(i)=mwZROT(i)+invsqICM(3,alpha)*LeviC(alpha,beta,dimns)*CMcoord(j,beta)*sqrt(mass1d(j))
      enddo
    enddo
    i=i+1
  enddo
enddo

! calculate translation eigenvectors
j=1
do i=1,natoms
  mwXTRANS(j)=sqrt(mass1d(i)/totalMass)
  j=j+1
  mwYTRANS(j)=sqrt(mass1d(i)/totalMass)
  j=j+1
  mwZTRANS(j)=sqrt(mass1d(i)/totalMass)
  j=j+1
enddo

! mass weight the gradient vector
do i = 1,nx
  mwgrad(I)=gradient(I)/sqrt(mass3d(I))
enddo

! Normalize the eigenvectors to be projected out of the mass weighted hessian
nmwgrad=mwgrad/sqrt(sum((mwgrad)**2))
nmwXROT=mwXROT/sqrt(sum((mwXROT)**2))
nmwYROT=mwYROT/sqrt(sum((mwYROT)**2))
nmwZROT=mwZROT/sqrt(sum((mwZROT)**2))
nmwXTRANS=mwXTRANS/sqrt(sum((mwXTRANS)**2))
nmwYTRANS=mwYTRANS/sqrt(sum((mwYTRANS)**2))
nmwZTRANS=mwZTRANS/sqrt(sum((mwZTRANS)**2))

! test prints of normalized eigenvectors to be projected from mass weighted hessian
!do i=1,nx
!  write(*,*),nmwgrad(i),nmwXROT(i),nmwYROT(i),nmwZROT(i),nmwXTRANS(i),nmwYTRANS(i),nmwZTRANS(i)
!enddo
! end test prints

! mass weight the hessian
do i = 1,nx
  do j=1,i
    mwhess(i,j) = hess(i,J) / SQRT(mass3d(i) * mass3d(j))
	mwhess(j,i) = mwhess(i,J)
!    write(*,*), mwhess(i,j)
  enddo
enddo

! calculate matrix P
do i=1,nx
  do j=1,nx
    dr_drt(i,j)=nmwgrad(i)*nmwgrad(j)+nmwXROT(i)*nmwXROT(j)+nmwYROT(i)*nmwYROT(j)+nmwZROT(i)*nmwZROT(j)+&
nmwXTRANS(i)*nmwXTRANS(j)+nmwYTRANS(i)*nmwYTRANS(j)+nmwZTRANS(i)*nmwZTRANS(j)
  enddo
enddo                                                                                                               

! set up identity matrix, I
unim=0.d0
do i=1,nx
  unim(i,i)=1.0d0
enddo 

! calculate (I-P)
do i=1,nx
  do j=1,nx
    pmat(i,j)=unim(i,j)-dr_drt(i,j)
  enddo
enddo

! Calculate projection (I-P)H(I-P)
pmhess=matmul(pmat,matmul(mwhess,transpose(pmat)))

End SUBROUTINE projection  

! define function for returning 3d Levi-Civita tensor elements
FUNCTION LeviC(i,j,k)
 INTEGER :: LeviC            
 INTEGER, INTENT( IN ) :: i,j,k
 LeviC = (j-i)*(k-i)*(k-j)/2
END FUNCTION LeviC
