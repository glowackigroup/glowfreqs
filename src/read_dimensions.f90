SUBROUTINE read_dimensions(report,ns)
implicit none

CHARACTER(LEN=40),INTENT(IN) :: report
INTEGER, INTENT(out) :: ns
character(len=80) :: dummy

OPEN(UNIT=8,FILE=report,POSITION="asis")

read (UNIT=8, FMT=*) dummy
read (UNIT=8, FMT=*) ns

END SUBROUTINE read_dimensions


